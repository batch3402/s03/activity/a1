INSERT INTO users (email, password, datetime_created)
VALUES 
( "johnsmith@gmail.com", "passwordA", current_timestamp()),
( "juandelacruz@gmail.com", "passwordB", current_timestamp()),
( "janesmith@gmail.com", "passwordC", current_timestamp()),
( "mariadelacruz@gmail.com", "passwordD", current_timestamp()),
( "johndoe@gmail.com", "passwordE", current_timestamp());

INSERT INTO posts ( author_id, title, content, datetime_posted) 
VALUES 
( 1, "First Code", "Hello World!", current_timestamp()),
( 1, "Second Code", "Hello Earth!", current_timestamp()),
( 2, "Third Code", "Welcome to Mars!", current_timestamp()),
( 4, "Fourth Code", "Bye bye solar system!", current_timestamp());

SELECT * FROM posts WHERE author_id = 1;

SELECT email, datetime_created FROM users; 

UPDATE posts SET content = "Hello to the people of the Earth!"  WHERE id = (SELECT id FROM posts WHERE content = "Hello Earth!");

DELETE FROM users WHERE email ="johndoe@gmail.com";